<!--=================================
     SOCIAL SECTION
==================================-->
<!-- social section -->
<div id="contact-page">
	<section id="social">
		<!-- social heading -->			
		<div class="container sc-contact hd-line">
			<div class="sixteen columns center-text">
				<h2><?= the_field('contact_title', '55') ?></h2>
				<p><?= the_field('contact_subtitle', '55') ?></p>
			</div>
		</div>
		<!-- end social heading -->			
		<!-- social subscribe form and twitter feed -->			
		<div class="container subscribe">
			<!-- contact form -->
			<div class="sixteen columns contactform">
				<?php
				//----------------------------------------------------------------------------------------.
				// [START] PHP E-mail Sending Validation (Name, Mail, Message) code - Thiago Moreno - v0.9|
				//----------------------------------------------------------------------------------------'

				/* DEFINITIONS
				================================================================ */
				DEFINE( 'DESTINY', get_field( 'contact_email', '55' ) );  // Sets recipient
				//DEFINE( 'DESTINY', 'thiago_moreno@quicktech.no' );  // Sets recipient
				DEFINE( 'FIELDCOUNT', 3 ); // Sets the quantity of fields which will be validated
				DEFINE( 'SMTP_USER', 'sender@studentpsykologen.no' );	// Sets the SMTP user for secure SMTP email sending
				DEFINE( 'SMTP_PASS', 'sender_!@#$%*()' ); // Sets the SMTP pass for secure SMTP email sending

				/* CREATING VARIABLES TO HANDLE FIELDS AND LOGIC
				================================================================ */
				$e_name = $e_mail = $e_msg = $commentErr = "";  // Error messages that will be displayed in <span class="error-msg"></span> in case of user's innocence
				$thename = $mail = $msg = "";          // Data that hopefully will be sent
				$invalidFields = FIELDCOUNT;           // Holds the number of invalid fields. This will be checked at the end of the request in order to allow ($invalidFields = 0) or deny ($invalidFields > 0) e-mail sending

				/* STARTING REQUEST
				================================================================ */
				if ( $_SERVER[ 'REQUEST_METHOD' ] == 'POST' && ! isset( $_POST[ 'changedPage' ] ) || isset( $_POST[ 'changedPage' ] ) && ( $_POST[ 'changedPage' ] == null || $_POST[ 'changedPage' ] == '0' ) ) 
				{
					//require_once( 'library/phpmailer/class.phpmailer.php' );

					// FIELD 1
					//Checking user's innocence when one inputs his codename and displaying a fearful warning if the one is mistaken
					if ( empty( $_POST[ 'thename' ] ) ) { $e_name = get_field( 'error_msg_1', '55' ) == '' ? 'Name is required' :  get_field( 'error_msg_1', '55' ); }
					else
					{
						$thename = pesv_filter_input( $_POST[ 'thename' ] );
						// Checking if codename has only letters and whitespace
						if ( !preg_match( '/^[a-zA-Z ]*$/', $thename ) ) /* ---> */ { $e_name = get_field( 'error_msg_2', '55' ) == '' ? 'Only letters and white space allowed' :  get_field( 'error_msg_2', '55' ); }
						else { $invalidFields--; } //Subtracts one error.
					}

					// FIELD 2
					//Checking user's innocence when one inputs its bottle address and displaying a fearful warning if the one is mistaken
					if ( empty( $_POST[ 'mail' ] ) ) { $e_mail = get_field( 'error_msg_3', '55' ) == '' ? 'Email is required' :  get_field( 'error_msg_3', '55' ); }
					else
					{
						$mail = pesv_filter_input( $_POST[ 'mail' ] );
						// Checking if bottle address has valid syntax for a e-mail address
						if ( !preg_match( '/([\w\-]+\@[\w\-]+\.[\w\-]+)/', $mail ) ) /* ---> */ { $e_mail = get_field( 'error_msg_4', '55' ) == '' ? 'Invalid e-mail format' :  get_field( 'error_msg_4', '55' ); }
						else { $invalidFields--; } //Subtracts one error.
					}

	 				// FIELD 3
					// Checking user's blabbery and seeking for one goddamn character at least
					if ( empty( $_POST[ 'msg' ] ) ) { $e_msg = get_field( 'error_msg_5', '55' ) == '' ? 'Write something!' :  get_field( 'error_msg_5', '55' ); } 
					else 
					{
						$msg = pesv_filter_input( $_POST[ 'msg' ] );
						$invalidFields--; //Subtracts one error.
					}

					/* SEND MAIL USING SECURE SMTP IF ALL FIELDS ARE VALID 
					 * Requires a working SMTP server
					================================================================ */
					if ( $invalidFields == 0 ) 
					{
						$to     =  DESTINY;
						$sub    = 'Studentpsykologen: Message from '   . $thename . ' (' . $mail . ')';
//						$head   = 'From: '                             . $mail . '\r\n' .
//						          "Reply-To: "                         . DESTINY . "\r\n" . 
//						          'X-Mailer: PHP/'                     . phpversion();
						$head[] = 'From: '                             . $mail;
						$head[] = "Reply-To: "                         . $mail;
						$head[] = 'X-Mailer: PHP/'                     . phpversion();
						
						
						try
						{		
							// echo "NO ERROR";
							if ( wp_mail( $to, $sub, $msg, $head ) )
							{
								$victory = true; //Message sending succeeded
							}
							else
							{
								$victory = false; //Message sending succeeded
							}
							
							$thename = $mail = $msg = $e_name = $e_mail = $e_message = '';
						}
						catch( Exception $e )
						{
							$victory = false;
							
//							$_e = $e->ErrorInfo;

							if ( WP_DEBUG ) echo $e;
						}


					}
					else 
					{
						$invalidFields = FIELDCOUNT; //Resets field counter
						$victory = false; //Message failed
					}
				}

				/* FILTERS RISKY CHARACTERS
				================================================================ */
				function pesv_filter_input($string)
				{
					$string = trim($string); //Removing extra spaces, tabs and new lines
					$string = stripslashes($string); //Removing backslashes
					$string = htmlspecialchars($string); //Swapping special characters with HTML entities
					return $string;
				}
				//--------------------------------------------------------------------------------------.
				// [end] PHP E-mail Sending Validation (Name, Mail, Message) code - Thiago Moreno - v0.9|
				//--------------------------------------------------------------------------------------'
				?>
				<form method="POST" action="">
					<div class="input">
						<?php 
						if ( isset( $victory ) && $victory == true ) 
						{ ?>
							<span class="victory">Message Sent Successfully</span>
						<?php 
						} ?>
						<span class="error-msg text-lefter <?php if ( $e_name != '' ) echo 'show-error-msg'; ?>"><?php echo $e_name ?></span>
						<input
							type="text"
							placeholder="<?= the_field('contact_name_placeholder', '55') ?>"
							name="thename"
							id="cm-name"
							class="<?php if ($e_name != ''){ echo 'show-error-msg-border';} ?>"
							value="<?php echo $thename; ?>"
						/>
					</div>
					<div class="input">
						<span class="error-msg text-lefter <?php if ( $e_mail != '' ) echo 'show-error-msg'; ?>"><?php echo $e_mail;?></span>
						<input
							type="text"
							placeholder="<?= the_field('contact_email_placeholder', '55') ?>"
							name="mail"
							id="cm-trj-trj"
							class="<?php if ( $e_mail != '' ) echo 'show-error-msg-border'; ?>"
							value="<?php echo $mail; ?>"
						/>
					</div>
					<div class="input">
						<span class="error-msg text-lefter <?php if ($e_msg != ''){ echo 'show-error-msg';} ?>"><?php echo $e_msg;?></span>
						<textarea
							class="textarea <?php if ($e_msg != ''){ echo 'show-error-msg-border';} ?>"
							cols="5"
							rows="4"
							id="cm-comment"
							placeholder="<?= the_field('contact_message_placeholder', '55') ?>"
							name="msg"><?php echo $msg; ?></textarea>
					</div>
					<div class="actions">
						<button type="submit" class="btn sendmsg large block" name="submit" id="submit"><?php the_field('send_message_button_label', '55') ?> <i class="icon-caret-right"></i></button>
					</div>
				</form>
			</div>
			<!-- end contact form -->
		</div>
		<!-- end social subscribe form and twitter feed -->
		<!-- social icons -->
		<div class="container center-text">
			<ul class="social">
				<?php if(get_field('show_facebook_image_and_link', '55')) { ?>
					<li><a href="https://www.facebook.com/<?= the_field('facebook_url', '55') ?>" target="_blank"><i class="icon-facebook"></i></a></li>
				<?php } ?>
				<?php if(get_field('show_twitter_image_and_link', '55')) { ?>
					<li><a href="https://www.twitter.com/<?= the_field('twitter_url', '55') ?>" target="_blank"><i class="icon-twitter"></i></a></li>
				<?php } ?>
				<?php if(get_field('show_instagram_image_and_link', '55')) { ?>
					<li><a href="http://www.facebook.com/<?= the_field('instagram_url', '55') ?>" target="_blank"><i class="icon-instagram"></i></a></li>
				<?php } ?>
				<?php if(get_field('show_pinterest_image_and_link', '55')) { ?>
					<li><a href="https://www.pinterest.com/<?= the_field('pinterest_url', '55') ?>" target="_blank"><i class="icon-pinterest"></i></a></li>
				<?php } ?>
			</ul>
		</div>
		<!-- end social icons -->
		<!-- contact details -->
		<div class="container center-text">
			<div class="contact-info center-text">
			  <ul>
				<li><i class="icon-phone"></i> <?php echo get_field('contact_telephone', '55') ?></li>
				<li><i class="icon-envelope"></i> <?php echo get_field('contact_email', '55') ?></li>
				<li><i class="icon-map-marker"></i> <?php echo get_field('contact_address', '55') ?></li>
			  </ul>
			</div>
		</div>
		<!-- end contact details -->
	</section>
	<!-- end social section -->
<?php
if ( ( ( $e_name != "" || $e_mail != "" || $e_msg != "" ) || ( isset( $victory ) && $victory == true ) ) /*&& $_POST[ "go_to" ] == ""*/ )
	{
	   echo '<script type="text/javascript">'
	   , 	'$(document).ready( function() '
	   , 	'{'
	   , 		'$( "html, body" ).animate( { scrollTop: $( "#contact-page" ).offset().top - 60 }, 500 );'
	   ,    '});'
	   , '</script>';
	}
	$victory = null;             // Reseting to avoid redirecting to contact everytime the above IF statement reads the $victory
?>
