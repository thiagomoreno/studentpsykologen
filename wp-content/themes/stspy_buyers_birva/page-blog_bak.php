<?php
/*
	Template Name: Blog
*/
?>

<!--=================================
     BLOG POST SECTION
==================================-->
<div id="blog-page">
	<div class="container">
		<!-- blog heading  -->
		<div class="sixteen columns menu-head">
			<h1><?= get_field( 'blog_title', '53' ); ?></h1>
			<p><?= get_field( 'blog_subtitle', '53' ); ?></p>
		</div>
		<div class="clear"></div>
		<div class="blog-main-wrapper">
			<!-- fullwidth -->
			<div  id="fullwidth">
				<?php
					wp_reset_postdata();
					/* SETTING WP_QUERY TO GET POSTS
					================================================== */
					$args = array(
						'post_type' => 'post',
						'post_status' => 'publish',
						'cat' => '1',
						'posts_per_page' => '2',
						'paged' => $paged,
					);
					$the_query = new WP_Query($args);
					$posts_array = array(); //Array to store posts

					/* THE LOOP :D
					================================================== */
					while ( $the_query->have_posts() ) 
					{
						$the_query->the_post();
						//$posts_array[] = get_post();
					
						// $content = $post->post_content;
						// $content = apply_filters('the_content', $content);
						// $content = str_replace(']]>',']]&gt;', $content);
						//echo substr(strip_tags($content),0,100);
						?>
						<!-- blog post -->
						<article style="box-shadow: 0px 2px 3px #aaa; moz-ox-shadow: 0px 2px 3px #aaa; webkit-box-shadow: 0px 2px 3px #aaa;" class="postlist postborder isotope-item">  
							<!-- content -->
							<div class="content-area">
								<!--thumb -->
								<div class="thumb">
									<a href="#" class="fade">
										<span class="imageCover"></span>
										<?php echo get_the_post_thumbnail(); ?>
									</a>
								</div>
								<!-- end thumb -->
								<!-- post detail -->
								<div class="postcontent">
									<div class="date"><div class="ribbon"><span><i class="icon-calendar"></i></span><?php echo date( 'j M, Y', strtotime( get_the_date() ) ) ?></div></div>
									&nbsp;
									<p class="posttitle"><a href="<?php the_permalink() ?>"><?php echo get_the_title() ?></a></p>
									<p><?php echo get_the_excerpt(); ?></p>
								</div>
								<!-- end post detail -->
								<!-- post meta -->
								<div class="post-meta">
									&nbsp;
									<a class="right" href="<?php the_permalink() ?>"><i class="icon-link"></i> Read more</a>
								</div>
								<!-- end post meta -->
							</div>
							<!-- end content -->
						</article>
						<!-- end blog post -->
						<?php
					}

				?>
				<p><a href="<?php next_posts_link('&laquo; Older Entries'); ?>">TESTE direita</a></p>
				<p><a href="<?php previous_posts_link('Newer Entries &raquo;') ?>">TESTE esquerda</a></p>
			</div>
			<!-- end fullwidth -->
			<div class="clearfix"></div>
		</div>
		<div class="clear"></div>
		<div class="pagination">
			<ul>
				<li><a href="#"><i class="icon-double-angle-left"></i></a></li>
				<li><a href="#"><i class="icon-angle-left"></i></a>
				</li><li><a class="current" href="#">1</a></li>
				<li><a href="#">2</a></li>
				<li><a href="#">3</a></li>
				<li><a href="#"><i class="icon-angle-right"></i></a></li>
				<li><a href="#"><i class="icon-double-angle-right"></i></a></li>
			</ul>
		</div>
		<div class="clear"></div>
	</div>
</div>
<!-- end Blog page -->
<div class="clearfix"></div>
<!-- parallax -->
<div id="parallax6" class="parallax">
	<div class="blog-teaser-bg parallax-bg">
		<div class="info-container">
			<div class="info">
				<div class="container">
					<div class="sixteen columns livet">
						<h2><?= the_field('blog_teaser_text', '53'); ?></h2>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end parallax -->
<div class="clear"></div>