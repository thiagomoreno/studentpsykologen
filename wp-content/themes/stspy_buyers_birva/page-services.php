<!--=================================
     SERVICES SECTION 
==================================-->
<div id="service-page">
	<div class="container">
		<!-- Service heading  -->
		<div class="sixteen columns menu-head">
			<h1><?php the_field('our_services_title', '49') ?></h1>
			<p><?php the_field('our_services_subtitle', '49') ?></p>
		</div>
		<div class="clear"></div>
		<!-- end Service heading  -->
		<div class="sixteen columns center-text">
			<div class="sixteen columns center-text">
				<div class="work-text wysiwyg_container"><?php the_field('our_services_description', '49') ?></div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="clearfix"></div>
<!-- end Service page -->
<!-- parallax -->
<div id="parallax2" class="parallax">
	<div class="service-teaser-bg parallax-bg">
		<div class="info-container">
			<div class="info">
				<div class="container">
					<h2><?php the_field('our_services_teaser_text', '49') ?></h2>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end parallax -->
<div class="clear"></div>