<!--=================================
         ABOUT US SECTION
==================================-->
<div id="about-page">
	<div class="container">
		<!-- about section header -->
		<div class="sixteen columns menu-head">
			<h1><?php echo get_field('about_us_title', '35') ?></h1>
			<p><?php echo get_field('about_us_subtitle', '35') ?></p>
		</div>
		<div class="clear"></div>
		<!-- end about section header -->
		<!-- about section description -->
		<div class="sixteen columns center-text">
			<div class="work-text wysiwyg_container"><?= the_field('about_us_description', '35') ?></div>
		</div>
		<div class="clear"></div>
		<!-- end about section description -->
		<!-- Video block -->
		<?php
		if ( get_field( 'show_vimeos_video', '35' ) )
		{ ?>
		<div class="ten columns video-about">
			<div class="content-area video">
				<iframe class="video-frame" width="100%" height="auto" src="<?php the_field('about_video','35');?>?title=0&amp;byline=0&amp;portrait=0"></iframe>
			</div>
		</div>
		<?php
		} ?>
		<!-- end Video block -->
		<div class="clear"></div>
	</diV>
</div>
<!-- end About section -->
<!-- parallax -->
<div id="parallax5" class="parallax">
	<div class="about-teaser-bg parallax-bg">
		<div class="info-container">
			<div class="info">
				<div class="container">
					<h2><?php the_field('about_teaser_text','35'); ?></h2>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end parallax -->
<div class="clear"></div>
<!-- end about us section -->