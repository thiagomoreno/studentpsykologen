<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till #main div
 *
 * @package Studentpsykologen
 * @since 1.0.0
 */
require_once (get_template_directory() . "/pages-content/head-file.php");
?>
	<body id="skrollr-body">
	<?php require_once(get_template_directory() . "/pages-content/header-file.php") ?>