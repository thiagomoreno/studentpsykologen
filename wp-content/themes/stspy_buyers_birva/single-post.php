<?php 
while ( have_posts() ) : the_post();

$content = get_the_content();
$content = apply_filters( 'the_content', $content );
$content = str_replace( ']]>',']]&gt;', $content );

$comments = get_comments( array( 'post_id' => $post->ID, 'status' => 'approve' ) );

//Thumbnail URL
$url = get_field( 'blog_post_image' );
//$url = wp_get_attachment_url( get_post_thumbnail_id() );

get_header(); 

//Check if is a BLOG post
$category = get_the_category();
if ( $category[0]->cat_name == "Blog Posts" ) {
?>


<!--==================================
             CONTENT
    ==================================-->
 <div id="content-holder">
	<!-- page bottom section -->
	<div class="content" >
		<!--==================================
             BLOG POST SINGLE PAGE 
		==================================-->
		<div id="blog-single-page">
			<div class="container">
				<!-- blog heading  -->
				<div class="sixteen columns menu-head">
					<h1><?php the_title(); ?></h1>
					<p><?php echo get_field('blog_post_subtitle'); ?></p>
					<!-- post meta detail -->
					<div class="post-metadetail">
						<span><i class="icon-calendar"></i> </span> <?php echo date( 'j M, Y', strtotime( get_the_date() ) ) ?>
					</div>
					<!-- end post meta detail -->
				</div>
				<div class="clear"></div>
				<div class="blog-main-wrapper">
					<div class="twelve columns">
						<!-- article -->
						<article>
							<!-- post body -->
							<section class="articleBody clearfix">
								<!-- image  -->
								<div class="content-area">
									<span class="imageCover"></span>
									<img  class="singleImage attachment-blog-featured" title="" alt="" src="<?php echo $url; ?>" ></img>
								</div>
								<!-- end image -->
								<div class="content-text">
									<?php										
										the_content('');
									?>
								</div>
							</section>
							<!-- end post body -->
							<!-- post tag -->
							<section class="article-footer clearfix">
								<p class="tags">
									<span class="tags-title">Tagged</span><br> 
									<?php
										$posttags = ( get_the_tags() ? get_the_tags() : array("name" => "Psykologen") );
										
										if ($posttags["name"] == "Psykologen") echo '<span rel="rag">| Psykologen |</span>';
										else
										{
											echo "| ";
											foreach($posttags as $tag) 
											{
												echo '<span rel="rag">' . $tag->name  . '</span> | '; 
											}
										}
									?>
								</p>
							</section>
							<!-- end post tag -->
							<!--  post respond -->
							<section>
								<?php
								if ( is_user_logged_in() ) 
								{
									global $current_user; get_currentuserinfo();
									echo '<p class="scope-area"> Logged in as <span class="scope-user-label">' . $current_user->user_login . '</span>. <a class="scope-logout-button" href="' . wp_logout_url( get_permalink() ) . '">Logout?</a></p>';
								} 
								?>
								<?php echo '<h3 class="reply-label">' . get_field( 'single_comments_area_reply_label', get_page_by_title( 'Single' ) ) . '</h3>'; ?>
								<div id="respond"> 
									<div class="message"></div>
									<?php
										$fields =  array
										(
										    'author' => '<div class="comment-form-author input">'.
										        '<input id="cm-name" name="author" type="text" value="" placeholder="' . get_field('single_comment_form_name_placeholder', get_page_by_title( 'Single' ) ) . '" size="30" /></div>',
										    'email'  => '<div class="comment-form-email input">'.
										        '<input id="cm-trj-trj" name="email" type="text" placeholder="' . get_field( 'single_comment_form_email_placeholder', get_page_by_title( 'Single' ) ) . '" value="" size="30"/></div>',
										);			 
										$comments_args = array( 'fields' =>  $fields );
										
										function wpsites_modify_comment_form_default($arg) {
										
										$arg['title_reply']          = '';
										$arg['logged_in_as']         = '';
										$arg['comment_field']        = '<div class="comment-form-comment input"><textarea id="comments" class="textarea" name="comment" placeholder="'. get_field( 'single_comment_form_comment_placeholder', get_page_by_title( 'Single' ) ) .'" value="" cols="5" rows="4"></textarea></div>';
										$arg['comment_notes_before'] = '';
										$arg['comment_notes_after']  = '';

										return $arg;
										}
										add_filter('comment_form_defaults', 'wpsites_modify_comment_form_default');

										comment_form($comments_args);
									?>		
								<!-- </div> -->
							</section>
							<!-- end post respond -->
							<!-- post comments -->
							<section class="post-comment">
								<h4> Comments (<?php echo get_comments_number(); ?>) </h4>
								<ul class="commentlist">
								<?php 
									// Comment Loop
									if ( $comments ) {
										foreach ( $comments as $comment ) 
										{ 
											?>
											<li> 
												<article class="comment">
													<header class="comment-author">
														<?php echo get_avatar( $comment->comment_author_email, '128' ); ?>                                       
													</header>
													<section class="comment-details <?php echo $comment->user_id == 1 ? 'admin-comment' : ''; ?>">
														<div class="author-name"> <a title="" href=""> <?php echo $comment->comment_author; ?> </a> </div>
														<div class="commentmetadata"> <?php echo $comment->comment_date; ?> </div>
														<div class="comment-body">
															<div class="comment-content">
																<?php echo $comment->comment_content; ?>
															</div>
														</div>
														<!--div class="reply"> <a title="" href=""> Reply » </a> </div-->
													</section>   
												</article>
											</li>
											<?php
										}
									}
								?>
								</ul>
							</section>
							<!-- end post comments -->
						</article>
						<!-- end article -->
					</div>
					<div class="four columns">
						<!-- author -->
						<div id="author">
							<?php echo get_avatar( $comment->comment_author_email, '128' ); ?>
							<span class="written"><?php echo get_field( 'written_by_label', get_page_by_title( 'Single' ) ) ?></span><br>
							<span class="the-author"><?php the_author_firstname(); ?> <?php the_author_lastname(); ?></span>
						</div>
						<!-- end author -->
						<!-- recent post -->
						<div id="recent-posts">
							<h2 class="widgettitle">Related Articles</h2>
							<ul>
							<?php
							//to use in the loop, list 5 post titles related to the tags of the current post
							$tags = wp_get_post_tags( get_the_ID() ); 

								$tag_ids = array();
								foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id; 
								$args=array
								(
									'tag__in' => $tag_ids,
									'post__not_in' => array( get_the_ID() ),
									'posts_per_page' => 5,
									'caller_get_posts' => 1,
									'cat' => get_cat_ID( 'Blog Posts' ),
								);
								$my_query = new WP_Query($args);
								if( $my_query->have_posts() )
								{
									while ($my_query->have_posts()) : $my_query->the_post(); 
										$numRelPosts++;
									?>
									<li>
										<p><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></p>
										<a href="<?php the_permalink() ?>"><span class="icon-quote-left"></span><span class="related-post-excerpt"><?php echo substr( strip_tags( get_the_content() ), 0, 85 ) ;?></span><span class="related-post-ellipsis"> (...)</span></a>
										<time class="related-post-time"><?php echo date( 'j M, Y', strtotime( get_the_date() ) ) ?></time>
									</li>
									<?php
									endwhile;
								}
								wp_reset_postdata();

							?>
							</ul>
						</div>
						<!-- end recent post -->
					</div>
					<div class="clear"></div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<!-- end Blog page -->
		<div class="clearfix"></div>
		<!-- parallax -->
		<div id="parallax6" class="parallax">
			<div class="blog-teaser-bg parallax-bg">
				<div class="info-container">
					<div class="info">
						<div class="container">
							<div class="sixteen columns livet">
								<div class="twitter_author"><h2><a href="<?php echo get_field( 'twitter_link', get_page_by_title ('Single') ) ?>" target="_blank">@<?php echo get_field( 'twitter_a', get_page_by_title ('Single') ) ?></a></h2></div>
								<img class="twitter_img left" src="images/twitter.png" alt="">
								<div id="tweets">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end parallax -->
		<div class="clear"></div>
	<!--==================================
             SOCIAL SECTION
		==================================-->
		<!-- social section -->
		<div id="contact-page">
			<section id="social">
				<?php /*
				<!-- social heading -->			
				<div class="container sc-contact hd-line">
					<div class="sixteen columns center-text">
						<h2>STAY CONNECTED</h2>
						<p>Bibendum eleifend quam eget fermentum. Nulla facilisi Augue aliquam augue vel odio faucibus luctus bibendum  <br/>quam eget fermentum. Nulla facilisi.</p>
					</div>
				</div>
				<!-- end social heading -->			
				<!-- social subscribe form and twitter feed -->	
					
				<div class="container subscribe">
					<!-- subscribe form -->
					<div class="sixteen columns contactform">			
						<form method="post">
							<div class="input">
								<input type="text" required="required" placeholder="Name" name="cm-name" id="cm-name" >
							</div>
							<div class="input">
								<input type="email" required="required" placeholder="Email address" name="cm-trj-trj" id="cm-trj-trj">
							</div>
							
							<div class="actions">
								<button type="submit" class="btn sendmsg large block" name="submit" >Subscribe <i class="icon-caret-right"></i></button>
							</div>
						</form>			
					</div>
					<!-- end subscribe form -->
				</div>
				*/ ?>
				<!-- end social subscribe form and twitter feed -->
				<!-- social icon -->
				<div class="container center-text">
					<ul class="social">
						<li><a href="#"><i class="icon-facebook"></i></a></li>
						<li><a href="#"><i class="icon-twitter"></i></a></li>
						<li><a href="#"><i class="icon-instagram"></i></a></li>
						<li><a href="#"><i class="icon-pinterest"></i></a></li>
					</ul>
				</div>
				<!-- end social icon -->
				<!-- contact detail -->
				<div class="container center-text">
					<div class="contact-info center-text">
					  <ul>
						<li><i class="icon-phone"></i> <?php echo get_field( 'contact_telephone', get_page_by_title( 'Contact' ) ) ?></li>
						<li><i class="icon-envelope"></i> <?php echo get_field( 'contact_email', get_page_by_title( 'Contact' ) ) ?></li>
						<li><i class="icon-map-marker"></i> <?php echo get_field( 'contact_address', get_page_by_title( 'Contact' ) ) ?></li>
					  </ul>
					</div>
				</div>
				<!-- end contact detail -->
			</section>
			<!-- end social section -->

<?php
	}
	else
	{
		?>
		<div id="teste" class="404-single-post-message">
			<h3> No post found. Please, check the URL or return to the blog section. </h3>
		</div>
		<?php
	}
	get_footer(); 
?>
<script type="text/javascript">
	$('#submit').addClass('btn sendmsg large block');
</script>
<?php
endwhile;
?>