				<!--
				==================================
					 FOOTER SECTION
				==================================
				-->
			</div>
			<div class="clearfix"></div>
			<!-- footer -->
			<div class="footer">
				<div class="container">
					<p><?php echo get_field( 'footer_info', '251' ) ?></p>
				</div>
			</div>
			<!-- end footer section -->
		</div>
		<div class="clearfix"></div>
		<!-- end page bottom -->
	</div>
	<!-- end Main wraper -->

	<?php
	/* CHANGING PARALLAX BACKGROUND IMAGES WITH JQUERY (There is no 
	/* way to do it using PHP without requiring wp_load.php again)
	======================================================== */
	echo '<style>'
	   , '.home-teaser-bg    { background-image: url("' . get_field( 'home_teaser_background_image', '29' ) . '") }'
	   , '.priser-teaser-bg  { background-image: url("' . get_field( 'the_priser_teaser_background_image', '279' ) . '") }'
	   , '.service-teaser-bg { background-image: url("' . get_field( 'our_services_teaser_background_image', '49' ) . '") }'
	   , '.about-teaser-bg   { background-image: url("' . get_field( 'about_teaser_background_image', '35' ) . '") }'
	   , '.blog-teaser-bg    { background-image: url("' . get_field( 'blog_teaser_background_image', '53' ) . '") }'
	   , '.team-teaser-bg    { background-image: url("' . get_field( 'team_teaser_background_image', '42' ) . '") }'
	   , '.work-teaser-bg    { background-image: url("' . get_field( 'work_teaser_background_image', '32' ) . '") }'
	   , '</style>';

	// $_POST[ "changedPage" ] = "0"; // Resets the variable responsible to detect if submit was from blog page or contact in order to avoid submitting all the forms
	?>
	</body>
</html>