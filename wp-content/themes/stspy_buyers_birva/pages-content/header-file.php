<!-- pre loader -->
<style media="only screen and (max-width: 480px)">
	#status { background-image: url('<?php the_field( "website_loading_image_mobile", "6" ); ?>') }
</style>
<style media="only screen and (min-width: 481px)">
	#status { background-image: url('<?php the_field( "website_loading_image", "6" ); ?>') }
</style>

<div id="preloader">
    <!-- <div id="status">&nbsp;</div> --><!-- /status -->
    <div id="stauts">
<!--     <iframe src="" frameborder="0">
    	
    	<!doctype html>
		<html>
		  <head>
		    <meta charset="utf-8">
		    <meta http-equiv="X-UA-Compatible" content="IE=edge">
		    <script src="https://www.gstatic.com/swiffy/v6.0/runtime.js"></script>

		    <script>
		      swiffyobject = {"internedStrings":["::::447d300c","::::689d447d","::::300c542c"],"tags":[{"bounds":[{"ymin":2540,"ymax":5459,"xmin":2540,"xmax":5459}],"id":1,"fillstyles":[{"color":[-14538439],"type":1}],"paths":[{"fill":0,"data":[":999c10zb76e:83i07db07d07d07d82ib:76e07D83ib07D07d83I07db83B:41E9Jb9X6J41D98Bb07D07D07D83Ib:75E07d82Ib07d07D82i07Dc::0Gb96B:68E5kb1Z1k63D13cb2T2t13C63db5K72b5K68eb:97b5k68eb1k2z13c64db2t2t63d13cb72b5k68e5kb97b:68e5Kb2z1K64d13Cb2t2T13c64Db5k71B5k68Eb:96B5K68Eb1K1Z13C63Db2T2T64D13Cb71B5K68E5Kc"]}],"flat":true,"type":1},{"id":1,"matrix":0,"type":3,"depth":1},{"bounds":[{"ymin":-932,"ymax":932,"xmin":-690,"xmax":690}],"id":2,"fillstyles":[{"color":[-1018334],"type":1}],"paths":[{"fill":0,"data":[":90f32Ib70Ed73I07db03D03d07D73ia38d:a7ceb1di7f9bb4h5fV1vaEcaBbaHkaHqaCiaDsaAnb:4f9e9jb9e5d2n5db3h:1n5Db9e5D9e9JaBTa:DaFRa:AaJTaMRa:gb6J6OV2Vb1d2C4j4Ca39d:a:39DbB3F5C4Jb9B7C6G7Cb9E:6N9eag:aRmaTjaA:aRfaD:aTbb4F:9J9Eb5D8E5D1Nb:3H5d2Nb5d9E9j9Eanaasdaicaqhakhabbaceb7h9e6n9eb7d:5g7Cb3c1D5c4Ja:38Dc"]}],"flat":true,"type":1},{"id":2,"matrix":"#2","type":3,"depth":2},{"bounds":[{"ymin":-690,"ymax":690,"xmin":-942,"xmax":942}],"id":3,"fillstyles":[{"color":[-9195859],"type":1}],"paths":[{"fill":0,"data":[":57D90Fa:38dbB3f5C4jb9B7c5G7cb9E:6N9EaCEaBBaKHaQHaICaSDaNAb4F:9J9eb5D9e5D2nb:3h5d1nb5d9e9j9eatBad:arFaa:atJarMaG:b7h9E6n9Eb7d:6g7cb3c1d5c4ja:39da48d:a7cEb1dI7f0Cb4h6FV2Va:gaMRaJTa:AaFRa:DaBTb:4F9e9Jb8e5D1n5Db3h:2n5db9e5d9e9jaAnaDsaCiaHqaHkaBbbD:Ecb6J6oV1vb1d3c4j5ca48d:bD73E10D76Ib06D04D80I04Dc"]}],"flat":true,"type":1},{"id":3,"matrix":"#0","type":3,"depth":3},{"bounds":[{"ymin":-942,"ymax":942,"xmin":-699,"xmax":700}],"id":4,"fillstyles":[{"color":[-12950943],"type":1}],"paths":[{"fill":0,"data":["::42Ib3H:1N5db9E5d9E9jabta:dafra:aajtamra:Gb6j6ov2vbZu7F0ca7Cea48D:a:48dae7cbi1d9b7fb9b7c6g7cb9e:6n9EaG:arMa:AatIaaAavFatAb4f:9j8eb5d9e5d2nb:3h5D2nb5D9e9J9eaNAaSDaICaQHaKHaBBb:DCEb7H9E6N9Eb7D:5G7cbTz9B7faE7ca:48dai:b76e:83i07Db07d07D07d83Ia:Ia48D:b3FB4J5Cb4H5Fv1VbaCeCabBahKahQacIadSaaNb:4F9E9Jb9E5D2N5Dc"]}],"flat":true,"type":1},{"id":4,"matrix":"#1","type":3,"depth":4},{"bounds":[{"ymin":-699,"ymax":700,"xmin":-932,"xmax":932}],"id":5,"fillstyles":[{"color":[-14991544],"type":1}],"paths":[{"fill":0,"data":[":32I99Fa:ib:74e04d80ib03d06d76i10da:48Dae7Cbi1D9b7Fb9b7C5g7Cb9e:6n9eaceabbakhaqhaicasdanab4f:9j9Eb5d9E5d2Nb:3H5D2Nb5D8E9J8EaTaaVfaAaaTia:aaRmag:b7H9e6N9eb7D:6G7CbTZ9B7FaE7Ca:48Da39D:b3Fb4J4cb4H6fv2va:Gamrajta:aafra:dabtb:4f9E9jb8E5d1N5db3H:2N5Db9E5D9E9JaaNadSacIahQahKabBaeCb6j6Ov1VbZT7F9Ba7CEc"]}],"flat":true,"type":1},{"id":5,"matrix":"::::542c689d","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"532D5288c5287C532D279c962d","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"7728B5360f5360F7728B016c235e","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"5536F0654h0654H5536F753b509e","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"8382J4071g4071G8382J89x782e","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"3964N5367d5366D3964N26v057f","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"1308P::1308P63s329f","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"521c520R520r521c922d748d","type":3,"depth":4},{"replace":true,"matrix":"5729E3199I3199i5729E68t220f","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"47p8568C8568c47p156e047e","type":3,"depth":4},{"replace":true,"matrix":"114x080s080S113x74u110f","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"425F9111E9111e425F389e348e","type":3,"depth":4},{"replace":true,"matrix":"3270I5206h5203H3268I79v001f","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"982T7610G7610g982T622e648e","type":3,"depth":4},{"replace":true,"matrix":"5497N5675C5673c5496N85w892e","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"0784D2296I2296i0784D856e949e","type":3,"depth":4},{"replace":true,"matrix":"693V4012G4008g695V90x783e","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"5536F1656J1656j5536F088f248f","type":3,"depth":4},{"replace":true,"matrix":"28s9173d9169D22s94y672e","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"783D5143c5142C783D687d991b","type":3,"depth":3},{"replace":true,"matrix":"8754b0671c0671C8754b995e128f","type":3,"depth":4},{"replace":true,"matrix":"0155L0429f0423F0149L701b563e","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"8019B4858f4858F8019B928d80z","type":3,"depth":3},{"replace":true,"matrix":"2198L8345g8345G2198L901e009f","type":3,"depth":4},{"replace":true,"matrix":"8550K9153E9144e8543K807b455e","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"5536F9783g9783G5536F167e70w","type":3,"depth":3},{"replace":true,"matrix":"0791L6397G6397g0791L807e888e","type":3,"depth":4},{"replace":true,"matrix":"785B5334D5329d794B912b345e","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"7802J3068g3068G7802J408e61t","type":3,"depth":3},{"replace":true,"matrix":"888u8440B8440b888u715e768e","type":3,"depth":4},{"replace":true,"matrix":"7945B5484f5474F7951B017c236e","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"2709N4641d4640D2709N646e50q","type":3,"depth":3},{"replace":true,"matrix":"5536F9616h9616H5536F622e648e","type":3,"depth":4},{"replace":true,"matrix":"274U9644e9635E281U075c174e","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"9566O::9566O887e40n","type":3,"depth":3},{"replace":true,"matrix":"8386N951Z951z8386N528e527e","type":3,"depth":4},{"replace":true,"matrix":"505O3373e3365E513O133c113e","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"61x236R236r61x033c201c","type":3,"depth":2},{"replace":true,"matrix":"5899E1582I1582i5899E791e65o","type":3,"depth":3},{"replace":true,"matrix":"918O8604F8604f918O435e407e","type":3,"depth":4},{"replace":true,"matrix":"422J6399d6389D431J192c053e","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"5Y7476C7476c5Y767b861b","type":3,"depth":2},{"replace":true,"matrix":"639v766r766R639v696e89p","type":3,"depth":3},{"replace":true,"matrix":"326Q6657f6657F326Q343e286e","type":3,"depth":4},{"replace":true,"matrix":"270F8983c8977C281F250c993d","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"754H6782E6782e754H99x21y","type":3,"depth":2},{"replace":true,"matrix":"2839I3885h3885H2839I599e12r","type":3,"depth":3},{"replace":true,"matrix":"1522N719x719X1522N248e167e","type":3,"depth":4},{"replace":true,"matrix":"081C1250c1244C092C307c931d","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"166W3806G3806g166W34v79u","type":3,"depth":2},{"replace":true,"matrix":"4334N5155C5155c4334N502e37s","type":3,"depth":3},{"replace":true,"matrix":"5538F7576g7576G5538F155e047e","type":3,"depth":4},{"replace":true,"matrix":"69H319w314W85H366c871d","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"2204D6997H6997h2204D68s38r","type":3,"depth":2},{"replace":true,"matrix":"272W3012G3012g272W407e61t","type":3,"depth":3},{"replace":true,"matrix":"2545E5069G5069g2545E104e981d","type":3,"depth":4},{"replace":true,"matrix":"52c560o558O36c425c810d","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"5536F5068I5068i5536F01q98n","type":3,"depth":2},{"replace":true,"matrix":"87j8560d8560D87j311e86u","type":3,"depth":3},{"replace":true,"matrix":"0158D0363G0363g0158D052e914d","type":3,"depth":4},{"replace":true,"matrix":"61d751m747M43d438c795d","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"7561bcC7561b08r34p","type":3,"depth":2},{"replace":true,"matrix":"9533K9742e9742E9533K215e09w","type":3,"depth":3},{"replace":true,"matrix":"8981B3681F3681f8981B999d847d","type":3,"depth":4},{"replace":true,"matrix":"67e974k971K49e452c782d","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"5536F1128i1128I5536F14s71q","type":3,"depth":2},{"replace":true,"matrix":"8009K8549E8549e8009K119e35x","type":3,"depth":3},{"replace":true,"matrix":"322S5299E5299e322S948d781d","type":3,"depth":4},{"replace":true,"matrix":"24f203j200J07f464c768d","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"4696OAa4696O21t08s","type":3,"depth":2},{"replace":true,"matrix":"348C4929D4929d348C024e59y","type":3,"depth":3},{"replace":true,"matrix":"426K5537D5537d426K895d714d","type":3,"depth":4},{"replace":true,"matrix":"34f436h434H17f478c754d","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"5537F7191H7191h5537F26u43t","type":3,"depth":2},{"replace":true,"matrix":"8233B4983f4983F8233B927d82z","type":3,"depth":3},{"replace":true,"matrix":"474E4747C4747c474E844d649d","type":3,"depth":4},{"replace":true,"matrix":"96e676f675F80e490c741d","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"687saA687s34v79u","type":3,"depth":2},{"replace":true,"matrix":"578U9231e9231E578U875d751b","type":3,"depth":3},{"replace":true,"matrix":"14O329W329w14O793d581d","type":3,"depth":4},{"replace":true,"matrix":"13e920d919D97d503c726d","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"5539F3253h3253H5539F41w16w","type":3,"depth":2},{"replace":true,"matrix":"810O3047e3047E810O821d819b","type":3,"depth":3},{"replace":true,"matrix":"89I691T691t89I780d565d","type":3,"depth":4},{"replace":true,"matrix":"91c227c225C75c517c714d","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"6820N::6820N47x53x","type":3,"depth":2},{"replace":true,"matrix":"716J6150d6150D716J768d889b","type":3,"depth":3},{"replace":true,"matrix":"99D063R063r99D770d551d","type":3,"depth":4},{"replace":true,"matrix":"42c32z31Z26c520c709d","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"5533F9316G9316g5533F53y88y","type":3,"depth":2},{"replace":true,"matrix":"538F8807c8807C538F716d956b","type":3,"depth":3},{"replace":true,"matrix":"5K427O427o5K758d535d","type":3,"depth":4},{"replace":true,"matrix":"87b00w99V71b524c707d","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"811k::811k60z725b","type":3,"depth":2},{"replace":true,"matrix":"309C1135c1135C309C664d025c","type":3,"depth":3},{"replace":true,"matrix":"3p789L789l3p746d522d","type":3,"depth":4},{"replace":true,"matrix":"0x52q51Q4v528c704d","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"5537F5379g5379G5537F767b861b","type":3,"depth":2},{"replace":true,"matrix":"48J252w252W48J610d094c","type":3,"depth":3},{"replace":true,"matrix":"34c155J155j34c734d507d","type":3,"depth":4},{"replace":true,"matrix":"4r06l05L9p531c700d","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"2881E3148g3148G2881E825b937b","type":3,"depth":2},{"replace":true,"matrix":"7v531o531O7v556d163c","type":3,"depth":3},{"replace":true,"matrix":"00d529G529g00d723d491d","type":3,"depth":4},{"replace":true,"matrix":"4l74h73H8j535c696d","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"0740D8760f8760F0740D886b012c","type":3,"depth":2},{"replace":true,"matrix":"49c725m725M49c545d179c","type":3,"depth":3},{"replace":true,"matrix":"75c957D957d75c711d477d","type":3,"depth":4},{"replace":true,"matrix":"4f31c30C9d537c693d","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"9712B2414f2414F9712B944b088c","type":3,"depth":2},{"replace":true,"matrix":"66d954k954K66d531d194c","type":3,"depth":3},{"replace":true,"matrix":"42c084D084d42c707d474d","type":3,"depth":4},{"replace":true,"matrix":":::P542c689d","type":3,"depth":5},{"type":2},{"replace":true,"matrix":"104T4365e4365E104T004c164c","type":3,"depth":2},{"replace":true,"matrix":"36e188j188J36e521d209c","type":3,"depth":3},{"replace":true,"matrix":"97b472C472c97b705d469d","type":3,"depth":4},{"type":2},{"replace":true,"matrix":"173L4910d4910D173L063c241c","type":3,"depth":2},{"replace":true,"matrix":"62e425h425H62e507d224c","type":3,"depth":3},{"replace":true,"matrix":"7y45Z45z7y701d466d","type":3,"depth":4},{"type":2},{"replace":true,"matrix":"108F4385c4385C108F124c316c","type":3,"depth":2},{"replace":true,"matrix":"39e670f670F39e494d240c","type":3,"depth":3},{"replace":true,"matrix":"2t36T36t2t699d460d","type":3,"depth":4},{"type":2},{"replace":true,"matrix":"74S164w164W74S182c391c","type":3,"depth":2},{"replace":true,"matrix":"68d921d921D68d483d255c","type":3,"depth":3},{"replace":true,"matrix":"5n12L12l5n695d456d","type":3,"depth":4},{"type":2},{"replace":true,"matrix":"01N560t560T01N194c409c","type":3,"depth":2},{"replace":true,"matrix":"62c224c224C62c471d270c","type":3,"depth":3},{"replace":true,"matrix":"4g06F06f4g692d452d","type":3,"depth":4},{"type":2},{"replace":true,"matrix":"64H961q961Q64H208c424c","type":3,"depth":2},{"replace":true,"matrix":"17c31z31Z17c467d275c","type":3,"depth":3},{"replace":true,"matrix":"#1","type":3,"depth":4},{"type":2},{"replace":true,"matrix":"31D353o353O31D221c441c","type":3,"depth":2},{"replace":true,"matrix":"7z98v98V7z464d279c","type":3,"depth":3},{"type":2},{"replace":true,"matrix":"4J738l738L4J234c456c","type":3,"depth":2},{"replace":true,"matrix":"3v51q51Q3v461d283c","type":3,"depth":3},{"type":2},{"replace":true,"matrix":"0l123j123J0l248c474c","type":3,"depth":2},{"replace":true,"matrix":"3q05l05L3q457d288c","type":3,"depth":3},{"type":2},{"replace":true,"matrix":"9w510g510G9w261c491c","type":3,"depth":2},{"replace":true,"matrix":"6k73h73H6k454d291c","type":3,"depth":3},{"type":2},{"replace":true,"matrix":"8z950d950D8z273c507c","type":3,"depth":2},{"replace":true,"matrix":"2f29c29C2f450d296c","type":3,"depth":3},{"type":2},{"replace":true,"matrix":"0y078d078D0y277c513c","type":3,"depth":2},{"replace":true,"matrix":"#0","type":3,"depth":3},{"type":2},{"replace":true,"matrix":"0v469c469C0v282c518c","type":3,"depth":2},{"type":2},{"replace":true,"matrix":"6s44z44Z6s285c522c","type":3,"depth":2},{"type":2},{"replace":true,"matrix":"7o33t33T7o289c526c","type":3,"depth":2},{"type":2},{"replace":true,"matrix":"4k11l11L4k293c533c","type":3,"depth":2},{"type":2},{"replace":true,"matrix":"9e05f05F9e296c538c","type":3,"depth":2},{"type":2},{"replace":true,"matrix":"#2","type":3,"depth":2},{"type":2},{"type":2},{"type":2},{"type":2}],"fileSize":3811,"v":"6.0.2","backgroundColor":-1,"frameSize":{"ymin":0,"ymax":8000,"xmin":0,"xmax":8000},"frameCount":59,"frameRate":36,"version":15};
		    </script>
		    <style>html, body {width: 100%; height: 100%}</style>
		  </head>
		  <body style="margin: 0; overflow: hidden">
		    <div id="swiffycontainer" style="width: 400px; height: 400px">
		    </div>
		    <script>
		      
		      var stage = new swiffy.Stage(document.getElementById('swiffycontainer'),
		                                   swiffyobject);
		      
		      stage.start();
		    </script>
		  </body>
		</html>

    </iframe> -->
    </div>
</div>

<div id="back-to-top">
   <a href="#">Back to Top</a> 
</div>
<!--==================================
         TOP HEADER
==================================-->
<section id="header-top">
	<div class="top-header">
		<div class="container">
			<div class="sixteen columns">
				<!-- logo -->
				<div class="left">
					<?php
					if ( get_field('image_logo', '29') ) 
					{
					?>
					<img src="<?php the_field( 'image_logo', '29' );?>" class="logo"></a>
					<?php
					}
					?>
				</div>
				<!-- end logo -->
				<!-- menu -->
				<div class="right" id="header" class="<?php if ( get_the_title( $post->post_parent ) != 'Studentpsykologen' ) echo 'menu-single'; ?>">
					<div class="menuwrapper clearfix">
						<div class="menu">
	 							<?php
	 							if ( get_the_title( $post->post_parent ) == 'Studentpsykologen' )
	 							wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); 
								// if ( get_the_title( $post->post_parent ) == 'Studentpsykologen' )
								// {
								// 	wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); 
								// }
								// else
								// {
								// 	wp_nav_menu( array( 'theme_location' => 'header-menu-single' ) );
								// }
								else
								{
									/*$menu_name = 'header-menu'; //e.g. primary-menu; $options['menu_choice'] in your case

									$menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
									$menu_items = wp_get_nav_menu_items( $menu->term_id );*/
								   	//require_once( dirname(__FILE__) . '/../menu-getter-single.php' );
								   	?>
								   	<ul class="menu-single">
										<li><a href="<?php bloginfo('siteurl'); ?>">Studentpsykologen</a></li>
									</ul>
									<?php
								}
								
							?>
						</div>
					</div>
				</div>
				<!--end menu -->
			</div>
		</div>
	</div>
	<div class="clear"></div>
</section>