<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till #main div
 *
 * @package Studentpsykologen
 * @since 1.0.0
 */
?>
<!DOCTYPE html>
<!--[if IE 7]><html class="no-js ie7 lt-ie9 lt-ie8" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 8]><html class="no-js ie8 lt-ie9" <?php language_attributes(); ?>><![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<!-- METAS ETC
	================================================== -->
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1,text/html" />
	<meta name="viewport" content="width=device-width, user-scalable=no, target-densitydpi=device-dpi"/>
	<meta name="description" content="Responsive One Page Template" />
	<meta name="keywords" content="html5,css3,jquery,portfolio,creative,design" />
	<meta name="author" content="Studentpsykologen" />
	<title><?php echo wp_title() . ' - ' . get_bloginfo('name'); ?></title>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<!-- Fav and Touch Icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/ico/apple-touch-icon-144-precomposed.jpg"/>
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/ico/apple-touch-icon-114-precomposed.jpg"/>
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/ico/apple-touch-icon-72-precomposed.jpg"/>
    <link rel="apple-touch-icon-precomposed" href="<?php echo get_template_directory_uri(); ?>/ico/apple-touch-icon-57-precomposed.jpg"/>
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/ico/favicon.jpg"/>
	<link href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" rel="shortcut icon" />
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/html5.min.js" type="text/javascript"></script>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<!-- FAVICON 
	================================================== -->
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri();?>/images/favicon.ico"/>

	<?php
		/* ENQUEUING FIRST-LOAD ASSETS
		=================================================== */
		//CSS
		wp_enqueue_style( 'googleFont1' );
		wp_enqueue_style( 'googleFont2' );
		wp_enqueue_style( 'baseCss' );
		wp_enqueue_style( 'skeletonCss' );
		wp_enqueue_style( 'componentCss', true );
		/*if ( is_single() )
			//wp_enqueue_style( 'component2Css' ); //slider*/
		wp_enqueue_style( 'fontAwesomeCss' );
		wp_enqueue_style( 'colorCss', true );
		wp_enqueue_style( 'customMoreno' );
		wp_enqueue_style( 'customFilipe' );
		//JS
		wp_enqueue_script( 'jQueryJs' );
		wp_enqueue_script( 'jQueryEasingMinJs' );
		wp_enqueue_script( 'retinaJs' );
		wp_enqueue_script( 'jQueryIsotopeMinJs' );
		wp_enqueue_script( 'masonryResponsiveJs' );
		wp_enqueue_script( 'jQueryParallaxJs' );
		wp_enqueue_script( 'jQueryFlexiselJs' );
		wp_enqueue_script( 'jQueryTweetableJs' );
		wp_enqueue_script( 'jQueryTimeagoJs' );
		wp_enqueue_script( 'jQueryCycleAllMinJs' );
		wp_enqueue_script( 'bellowsJs' );
		wp_enqueue_script( 'parallaxInitJs' );
		wp_enqueue_script( 'jQueryFitvidsJs' );
		wp_enqueue_script( 'scriptsJs' );
		// wp_enqueue_script( 'switcherJs' );
	?>
	<?php wp_head(); ?>
	<script>

/*	jQuery(document).ready(function()
	{
		// ajax pagination
		jQuery('.pagination ul li a').on('click', function()
		{ // if not using wp-page-numbers, change this to correct ID
			var link = jQuery(this).attr('href');
			// #main is the ID of the outer div wrapping your posts
			jQuery('#fullwidth-wrapper').html('loading');
			// #entries is the ID of the inner div wrapping your posts
			jQuery('#fullwidth-wrapper').load(link+' #fullwidth')
		});
	}); // end ready function*/

	/*jQuery( document ).ready( function()
	{
		var ajaxurl = 'http://wpstpsy.local/wp-admin/admin-ajax.php';
		
		jQuery( '.pagination ul li a' ).on( 'click', function( event )
		{
			event.preventDefault();


			var link = jQuery( this ).attr( 'href' );

			jQuery.post
			(
				ajaxurl,
				{
					action : 'loadBlogPage',
				}, 
				function( data )
				{
					$( '#fullwidth-wrapper' ).empty();

					jQuery( '#fullwidth-wrapper' ).load( link + ' #fullwidth', function()
					{
						jQuery( '#status-blog' ).css( "height","20px" );
						jQuery( '#status-blog' ).fadeOut( 350 ).css( 'display', 'none' );
						//jQuery( '#fullwidth' ).css( "", "" );

						jQuery( '#fullwidth' ).fadeIn( 350 );
					});
				}
			);
		});
	});*/
/*	jQuery( document ).ready( function()
	{
		var ajaxurl = 'http://wpstpsy.local/wp-admin/admin-ajax.php';
		
		jQuery( '.pagination ul li a' ).on( 'click', function( event )
		{
			event.preventDefault();


			var link = jQuery( this ).attr( 'href' );

			jQuery.post
			(
				ajaxurl,
				{
					action : 'loadBlogPage',
				}, 
				function( data )
				{
					$( '#fullwidth-wrapper' ).empty();

					jQuery( '#fullwidth-wrapper' ).load( link + ' #fullwidth', function()
					{
						jQuery( '#status-blog' ).css( "height","20px" );
						jQuery( '#status-blog' ).fadeOut( 350 ).css( 'display', 'none' );
						//jQuery( '#fullwidth' ).css( "", "" );

						jQuery( '#fullwidth' ).fadeIn( 350 );
					});
				}
			);
		});

		// jQuery.getScript("<?php echo SCRIPTS . 'jquery.isotope.min.js'; ?>", function(data, textStatus, jqxhr) {
		//    //console.log(data); //data returned
		//    //console.log(textStatus); //success
		//    //console.log(jqxhr.status); //200
		//    //console.log('script.js loaded.');
		// });
	});*/
/*	jQuery( document ).ready( function()
	{
		// jQuery( '.pagination ul li a' ).on( 'click', function( e )
		// {
			
		// 	jQuery( '#pagination' ).submit();
		// 	e.preventDefault();
		// });
	});*/
/*	jQuery( document ).ready( function()
	{
		alert("1");
		jQuery( '.pagination ul li a' ).on( 'click', function( event )
		{
			//event.preventDefault();
			alert("teste");
			jQuery.post()
			$.ajax
			({
				url: "<?php echo get_template_directory_uri() . '/front-page.php'; ?>",
				type: "POST",
				data: { testest: "test" },
				success: function( response )
				{
				      alert( url );
				},
				error: function()
				{
				      // do action
				}
				//alert(url);
			});
		});
	});*/
	/*jQuery( document ).ready( function()
	{
		var ajaxurl = 'http://wpstpsy.local/wp-admin/admin-ajax.php';
		jQuery.post
		(
			ajaxurl,
			{
				action : 'loadBlogPAge',
			}, 
			function( data )
			{
				// alert( data );
			}
		);

		jQuery( '.pagination ul li a' ).on( 'click', function( event )
		{
			event.preventDefault();

			// var docHeadObj    = document.getElementsByTagName( "head" )[0];
			// var dynamicScript = document.createElement( "script" );
			var link = jQuery( this ).attr( 'href' );

			jQuery( '#fullwidth' ).fadeOut( 350, function()
			{
				jQuery( '#status-blog' ).fadeIn( 350 ).css( 'display', 'block' );

				setTimeout( function() 
				{
					jQuery( '#fullwidth' ).load( link + ' #fullwidth', function()
					{
						jQuery( '#status-blog' ).css( "height","200px" );
						jQuery( '#status-blog' ).fadeOut( 350 ).css( 'display', 'none' );
						jQuery( '#fullwidth' ).css( "", "" );

						jQuery( '#fullwidth' ).fadeIn( 350 );
					});
				}, 1000 );
			});

			// dynamicScript.type = "text/javascript";
			// dynamicScript.src  = '../js/jquery.isotope.min.js';
			// docHeadObj.appendChild(dynamicScript);
		});
	});*/
	/*
	jQuery( document ).ready( function()
	{
		var ajaxurl = 'http://wpstpsy.local/wp-admin/admin-ajax.php';
		jQuery.post
		(
			ajaxurl,
			{
			  action : 'faz_o_bagulho',
			}, 
			function( data ) 
			{  
				jQuery( '.pagination ul li a' ).on( 'click', function( e )
				{

					e.preventDefault();

					var link = jQuery( this ).attr( 'href' );

					jQuery( '#fullwidth' ).fadeOut( 350, function()0
						jQuery( '#status-blog' ).fadeIn( 350 ).css( 'display', 'block' );

						setTimeout( function() 
						{
							jQuery( '#fullwidth' ).load( link + ' #fullwidth', function()
							{
								$("body").trigger("runScripts");
								$('#fullwidth').trigger('update');
								//jQuery( '#status-blog' ).css( "height","20px" );
								jQuery( '#status-blog' ).fadeOut( 350 ).css( 'display', 'none' );
								jQuery( '#fullwidth' ).css( "", "" );

								jQuery( '#fullwidth' ).fadeIn( 350 );
							});
						}, 1000 );
					});
				});
			}
		);

	});
	jQuery( document ).ready( function( $ )
	{
		var ajaxurl = 'http://wpstpsy.local/wp-admin/admin-ajax.php';

		$( '.pagination ul li a' ).on( 'click', function( e )
		{
			e.preventDefault();
			
			var link = jQuery( this ).attr( 'href' );

			$.post( ajaxurl, { action: 'faz_o_bagulho' }, function( data )
			{
				$( '#fullwidth' ).fadeOut( 250 ).empty().append( data ).fadeIn( 250 );
			});
		});
	}); 
	function LoadMyJs(url)
	{
		e.preventDefault();

		var docHeadObj    = document.getElementsByTagName( "head" )[0];
		var dynamicScript = document.createElement( "script" );

		dynamicScript.type = "text/javascript";
		dynamicScript.src  = url;

		docHeadObj.appendChild(dynamicScript);
	} 
*/
	</script>
</head>