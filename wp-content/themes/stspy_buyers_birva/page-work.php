	<!--==================================
             PORTFOLIO/WORK SECTION
		==================================-->
		<div id="work-page">
			<div class="container static">
				<!-- heading -->
				<div class="sixteen columns menu-head">
					<h1><?php echo get_field( 'our_work_title', '32' ); ?></h1>
					<p><?php echo get_field( 'our_work_subtitle', '32' ); ?></p>
				</div>
				<div class="clear"></div>
				<!-- description -->
				<div class="sixteen columns center-text">
					<div class="work-text wysiwyg_container"><?php echo get_field('our_work_text', '32'); ?></div>
				</div>
				<div class="clear"></div>
				<!-- portfolio grid -->
				<div class="portfolio-list">
				<?php
					wp_reset_postdata();
					/* SETTING WP_QUERY TO GET POSTS
					================================================== */
					$args = array(
						'post_type'   => 'post',
						'post_status' => 'publish',
						'orderby'     => 'date',
						'order'       => 'ASC',
						'cat'         => '3'
					);
					$the_query = new WP_Query($args);
					$posts_array = array(); //Array to store posts

					/* STORING POSTS IN $posts_array[]
					================================================== */
					while ( $the_query->have_posts() ) 
					{
						$the_query->the_post();
						$posts_array[] = get_post();
					}
					/* CREATING POSTS FROM $posts_array[]
					================================================== */
					foreach ($posts_array as $post)
					{
						$dataItem < 3 ? $dataItem++ : $dataItem = 1;
					?>
					<div class="one-third column">
						<!-- item block -->
						<div data-item="<?php echo $dataItem; ?>" class="portfolio-item-block work">
							<!-- item info -->
							<div class="info clearfix">
								<div class="content-area">
								<span class="imageCover"></span>
								<?php echo get_the_post_thumbnail($post->ID); ?>
								</div>
								<h4 class="title"><?php echo $post->post_title; ?></h4>
								<span class="sub-title"><?php echo get_field('our_work_box_subtitle'); ?></span>
							</div>
							<!-- end item info -->
							<!-- item detail -->
							<div class="details">
								<div class="container">
									<div class="eight columns">
										<?php echo get_the_post_thumbnail($post->ID); ?>
									</div>
									<div id="expandedContent" class="eight columns">
										
										<h2><?php echo $post->post_title; ?></h2>
										<?php echo get_field('our_work_expanded_box_text'); ?>
										<?php
											if ( get_field( 'our_work_box_toggle_visit_button' ) == "Show" )
											{
												$visitButtonLink = get_field( 'our_work_expanded_box_visit_page_button_link' ) == '' ? "#" : get_field( 'our_work_expanded_box_visit_page_button_link' );
												?>
												<a class="btn btn-large btn-block btn-first" href="<?php echo $visitButtonLink; ?>">&nbsp;<?php echo get_field( 'our_work_expanded_box_visit_page_button_label' ); ?>&nbsp;</a>
												<?php
											} 
										?>
										<a class="btn btn-large btn-block btn-close" href="#">&nbsp;<i class="icon-long-arrow-up"></i>&nbsp;</a>
									</div>
								</div>
								<div class="clear"></div>
							</div>
							<div class="clear"></div>
							<!-- end item detail -->
						</div>
						<!-- end item block -->
					</div>
					<?php
					}
				?>
				<div class="clear"></div>
				</div>
				<!-- end portfolio grid -->
			</div>
		<div class="clear"></div>
		<!-- end section header -->
		<!-- parallax -->
		<div id="parallax4" class="parallax">
			<div class="work-teaser-bg parallax-bg">
				<div class="info-container">
					<div class="info">
						<div class="container">
						<h2><?php echo get_field('our_work_teaser_text', '32') ?></h2>
						<?php /*
								<h2><?php //the_field('work_teaser_text', '32') ?></h2>
								<!-- testimonial slider -->
								<ul id="testimonial-slider">
									<!--?php
										wp_reset_postdata();
										$args = array(
											'post_type'   => 'post',
											'post_status' => 'publish',
											'orderby'     => 'date',
											'order'       => 'ASC',
											'cat'         => '4'
										);
										$the_query = new WP_Query($args);
										$posts_array = array(); //Array to store posts

										while ( $the_query->have_posts() ) 
										{
											$the_query->the_post();
											$posts_array[] = get_post();
										}
										foreach ($posts_array as $post)
										{
									?-->
										<li>
											<div class="testimonial">
												<!-- testimonial client photo and logo -->
												<div class="testimonial-image">
													<div class="bubble-image">
														<img width="264" height="264" alt="" src="<?php echo get_field('client_image_url') ?>">
													</div>
												</div>
												<!-- end testimonial client photo and logo -->
												<!-- testimonial client information -->
												<div class="testimonial-text">
													<h4 class="author nomargin"><?php echo get_field('client_name') ?></h4>
													<h5><?php echo get_field('client_position') ?></h5>
													<p><?php echo get_field('client_testimonial') ?></p>
													
												</div>
												<!-- end testimonial client information -->
											</div>
										</li>
									<!--?php
										} // end CREATING POSTS FROM $posts_array[]
									?-->
								</ul>	
								<!-- end testimonial slider --> */ ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end parallax -->
		<div class="clear"></div>
			<!-- end container -->