<!--=================================
     PRISER SECTION 
==================================-->
<div id="priser-page">
	<div class="container">
		<!-- Priser heading  -->
		<div class="sixteen columns menu-head">
			<h1><?php the_field('priser_title', '279') ?></h1>
			<p><?php the_field('priser_subtitle', '279') ?></p>
		</div>
		<div class="clear"></div>
		<!-- end Priser heading  -->
		<div class="sixteen columns center-text">
			<div class="work-text wysiwyg_container"><?php the_field('priser_description', '279') ?></div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<div class="clearfix"></div>
<!-- end Priser page -->
<!-- parallax -->
<div id="parallax2" class="parallax">
	<div class="priser-teaser-bg parallax-bg">
		<div class="info-container">
			<div class="info">
				<div class="container">
					<h2><?php echo get_field( 'priser_teaser_text', '279' ) ?></h2>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end parallax -->
<div class="clear"></div>