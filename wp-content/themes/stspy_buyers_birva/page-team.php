<!--==================================
     OUR TEEM SECTION
==================================-->
<div id="ourteem-page">
	<div class="container">
		<!-- our team heading  -->
		<div class="sixteen columns menu-head">
			<h1><?= the_field('team_title', '42') ?></h1>
			<p><?= the_field('team_subtitle', '42') ?></p>
		</div>
		<div class="clear"></div>
		<!-- our team description  -->
		<div class="sixteen columns center-text">
			<div class="work-text wysiwyg_container"><?= the_field('team_description', '42') ?></div>
		</div>										
		<div class="clear"></div>
		<div class="inner-main-wrapper">
			<!-- Team slider  -->
			<ul id="teem-slider">
				<?php
					wp_reset_postdata();
					/* SETTING WP_QUERY TO GET 'TEAM MEMBERS' POSTS
					================================================== */
					$args = array(
						'post_type'   => 'post',
						'post_status' => 'publish',
						'orderby'     => 'date',
						'order'       => 'ASC',
						'cat'         => '5'
					);
					$the_query = new WP_Query($args);
					$posts_array = array(); //Array to store posts

					/* STORING POSTS IN $posts_array[]
					================================================== */
					while ( $the_query->have_posts() ) 
					{
						$the_query->the_post();
						$posts_array[] = get_post();
					}
					/* CREATING POSTS FROM $posts_array[]
					================================================== */
					foreach ($posts_array as $post)
					{
				?>
				<li>
					<div class="one-third column setwidth animation-fade">
						<!-- team block  -->
						<div class="team-member clearfix" id="member1">
							<!-- Name  -->
							<h3 class="name"><?php echo get_field('team_member_name') ?></h3>
							<!-- Image  -->
							<div class="photo">
								<span class="imageCover"></span>
								<img alt="John Doe" class="member-image" src="<?php echo get_field('team_member_photo') ?>">		
							</div>
							<!-- designation  -->
							<p class="m-title"><?= get_field('team_member_job_position') ?></p>
							<!-- Description  -->
							<div class="desc">
								<p><?php echo get_field('team_member_short_description') ?></p>
							</div>
							<!-- Team member biography  -->
							<div class="team-member-bio">
								<p><?php echo get_field('team_member_bio') ?></p>
							</div>
							<!-- social link -->
							<div class="sociallink">
								<ul class="team-social">
									<?php if(get_field('show_facebook_link')) { ?>
										<li class="facebook">
											<a target="_blank" href="https://www.facebook.com/<?php echo get_field('team_member_facebook_url'); ?>"><i class="icon-facebook"></i></a>
										</li>
									<?php } ?>
									<?php if(get_field('show_googleplus_link')) { ?>
										<li class="googleplus">
											<a target="_blank" href="https://plus.google.com/<?php echo get_field('team_member_googleplus_url'); ?>"><i class="icon-google-plus"></i></a>
										</li>
									<?php } ?>
									<?php if(get_field('show_twitter_link')) { ?>
										<li class="twitter">
											<a target="_blank" href="https://twitter.com/<?php echo get_field('team_member_twitter_url'); ?>"><i class="icon-twitter"></i></a>
										</li>
									<?php } ?>
								</ul>
							</div>
							<!-- end social link -->
							<div style="clear:both"></div>
						</div>
						<!-- end team block  -->
					</div>
				</li>
				<?php
					} // end CREATING POSTS FROM $posts_array[]
				?>
			</ul>
			<!-- end team slider -->
		</div>
		<div class="clear"></div>
	</div>
</div>
<!-- end Team section -->
<!-- parallax -->
<div id="parallax6" class="parallax">
	<div class="team-teaser-bg parallax-bg">
		<div class="info-container">
			<div class="info">
				<div class="container">
						<h2><?php echo get_field('team_teaser_text', '42') ?></h2>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end parallax -->
<div class="clear"></div>

<!-- Mini-biography about each team member -->
<script language="javascript">
	$('.team-member').click(function()
		{
			$(this).find('.team-member-bio').slideToggle();
		}
	)
</script>