<!--==================================
         BANNER 
==================================-->
<div class="banner" id="parallax1">
	<!-- parallax background -->
	<div class="home-teaser-bg home-bg">
		<div class="info-container">
			<div class="info">
				<div class="container">
					<div class="home-inner-wrap">
						<h1 class="m-text">
						<!-- <?php echo "<h2>".$_SERVER['HTTP_USER_AGENT']."</h2>"; ?> -->
							<?php echo get_field( 'home_teaser_text_line_1', '29' ) != '' ? '<span>' . get_field( 'home_teaser_text_line_1', '29' ) . '</span>' : ''; ?>
						</h1>
						<div class="w-icon">
							<a href="#about-page" class="w-icon-link">
								<i class="icon-long-arrow-down"></i>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
</div>
	<!-- end parallax background -->
</div>
<div class="clear"></div>
<!-- end -->
<!--==================================
         CONTENT
==================================-->
<div id="content-holder">
	<!-- page bottom section -->
	<div class="content">
	<!--==================================
	       KNOW MORE SECTION (HOME)
	==================================-->
	<?php /*
	<div id="basics">
			<div class="container sc-know-more">
				<div class="one-third column inner-row">
					<div class="about-box-content">
						<!-- icon -->
						<h2><i class="icon-<?php echo str_replace( 'fa-', '', get_field( 'home_box_1_icon_name','29' ) ); ?>"></i></h2>
						<!-- end icon -->
						<h4><?php the_field('home_box_1_title','29'); ?></h4>
						<p><?php the_field('home_box_1_text','29');  ?></p>
					</div>
				</div>
				<div class="one-third column inner-row">
					<div class="about-box-content">
						<!-- icon -->
						<h2><i class="icon-<?php echo str_replace( 'fa-', '', get_field( 'home_box_2_icon_name','29' ) ); ?>"></i></h2>
						<!-- end icon -->
						<h4><?php the_field('home_box_2_title','29'); ?></h4>
						<p><?php the_field('home_box_2_text','29'); ?>.</p>
					</div>
				</div>
				<div class="one-third column inner-row">
					<div class="about-box-content">
						<!-- end icon -->
						<h2><i class="icon-<?php echo str_replace( 'fa-', '', get_field( 'home_box_3_icon_name','29' ) ); ?>"></i></h2>
						<!-- end icon -->
						<h4><?php the_field('home_box_3_title','29'); ?></h4>
						<p><?php the_field('home_box_3_text','29'); ?></p>
					</div>
				</div>
				<div class="clear"></div>
			</div>
	</div> */ ?>
	<div class="clear"></div>