<?php
	
	/* URL SHORTENINGS
	=================================================== */
	define("IMAGES", get_template_directory_uri().'/images/');
	define("SCRIPTS", get_template_directory_uri().'/scripts/');
	define("CSS", get_template_directory_uri().'/css/');

	/* OPTIONS
	=================================================== */
	// Enabling post thumbnails;
	add_theme_support( 'post-thumbnails' );
	// Enabling page exceprts
	add_action('init', 'enable_page_excerpts');
	function enable_page_excerpts()
	{
		add_post_type_support('page', 'excerpt');
	}

	/* REGISTERING STYLES
	=================================================== */
	// Google Fonts
	wp_register_style('googleFont1',         'http://fonts.googleapis.com/css?family=Oswald:300,400,700');
	wp_register_style('googleFont2',         'http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,700');
	// CSS Skeleton - Responsive Framework
	wp_register_style('baseCss',        CSS. 'base.css');
	wp_register_style('skeletonCss',    CSS. 'skeleton.css');
    //CSS Custom
	wp_register_style('componentCss',   CSS. 'component.css');
	wp_register_style('component2Css' , CSS. 'component2.css');
	wp_register_style('fontAwesomeCss', CSS. 'font-awesome.min.css');
	wp_register_style('colorCss',       CSS. 'colors/color3.css');
	wp_register_style('customMoreno',   CSS. 'custom-moreno.css');
	wp_register_style('customFilipe',   CSS. 'custom-filipe.css');

    /* REGISTERING JAVASCRIPTS
	=================================================== */
	wp_register_script('jQueryJs',			 	  SCRIPTS. 'jQueryv1.8.3.js');
	wp_register_script('jQueryEasingMinJs',	      SCRIPTS. 'jquery.easing.min.js');
	wp_register_script('retinaJs',                SCRIPTS. 'retina.js');
	wp_register_script('jQueryIsotopeMinJs',      SCRIPTS. 'jquery.isotope.min.js');
	wp_register_script('masonryResponsiveJs',     SCRIPTS. 'masonryResponsive.js');
	wp_register_script('jQueryParallaxJs',		  SCRIPTS. 'jquery.parallax-1.1.3.js');
	wp_register_script('jQueryFlexiselJs',		  SCRIPTS. 'jquery.flexisel.js');
	wp_register_script('jQueryTweetableJs',		  SCRIPTS. 'tweetable.jquery.js');
	wp_register_script('jQueryTimeagoJs',		  SCRIPTS. 'jquery.timeago.js');
	wp_register_script('jQueryCycleAllMinJs',     SCRIPTS. 'jquery.cycle.all.min.js');
	wp_register_script('bellowsJs',		          SCRIPTS. 'bellows.js');
	wp_register_script('parallaxInitJs',	      SCRIPTS. 'parallaxInit.js');
	wp_register_script('jQueryFitvidsJs',	      SCRIPTS. 'jquery.fitvids.js');
	wp_register_script('scriptsJs',		          SCRIPTS. 'scripts.js');
	//wp_register_script('switcherJs',		      SCRIPTS. 'switcher.js');

	/* REGISTERING MENUS
	=================================================== */
	// Main
	function register_my_menu() 
	{
  		register_nav_menu('header-menu',__( 'Header Menu' ));
	}
	add_action( 'init', 'register_my_menu' );
	// Single
	function register_my_menu2() 
	{
  		register_nav_menu('header-menu-single',__( 'Header Menu Single' ));
	}
	add_action( 'init', 'register_my_menu2' );

	/* AJAX PAGINATION
	=================================================== */
	/*add_action( 'wp_ajax_faz_o_bagulho', 'faz_o_bagulho' );
	add_action( 'wp_ajax_nopriv_faz_o_bagulho', 'faz_o_bagulho' );
	function faz_o_bagulho() { }
	function add_myjavascript()
	{
		wp_enqueue_script( 'ajax-pagination.js', get_bloginfo('template_directory') . "/scripts/ajax-pagination.js", array( 'jquery' ) );
	}
	add_action( 'init', 'add_myjavascript' );
	function MyAjaxFunction()
	{
		//get the data from ajax() call
		$GreetingAll = $_POST['GreetingAll '];
		$results = "<h2>".$GreetingAll."</h2>";
		// Return the String
		die($results);
	}
	// creating Ajax call for WordPress
	add_action( 'wp_ajax_nopriv_MyAjaxFunction', 'MyAjaxFunction' );
	add_action( 'wp_ajax_MyAjaxFunction', 'MyAjaxFunction' );*/
	add_action( 'wp_ajax_loadBlogPage', 'loadBlogPage' );
	add_action( 'wp_ajax_nopriv_loadBlogPage', 'loadBlogPage' );
	function loadBlogPage()
	{
		// Set up the paged variable
		$paged = ( isset( $_GET['pg'] ) && (int)$_GET['pg'] > 0 ) ? (int)$_GET['pg'] : 1;
		query_posts( array( 'post_type' => 'post', 'paged' => $paged, 'posts_per_page' => get_field( 'blog_number_of_posts_per_page', '53' ), 'cat' => 1 ) );
		?>
		<?php
		while ( have)
		?>
	<?php
	}

	/* ENABLE ANCHOR POINTS FOR THE DEFAULT WYSIWYG 
	=================================================== */
	function set_tinymce_buttons( $initArray ) 
	{
    	$initArray['theme_advanced_buttons1'] .= ',anchor';
    	return $initArray;
	}
	add_filter('tiny_mce_before_init', 'set_tinymce_buttons');

	/* ENABLE ANCHOR POINTS FOR THE ACF WYSIWYG 
	=================================================== */
	function my_toolbars( $toolbars )
	{
		// Uncomment to view format of $toolbars
		/*
		echo '< pre >';
			print_r($toolbars);
		echo '< /pre >';
		die;
		*/
	 
		// Add a 'anchor' item to the first line of the WYSIWYB editor
		array_push( $toolbars['Full' ][1], 'anchor' );
	 
		// Edit the "Full" toolbar and remove 'code'
		// - delet from array code from http://stackoverflow.com/questions/7225070/php-array-delete-by-value-not-key
		if( ($key = array_search('code' , $toolbars['Full' ][2])) !== false )
		{
		    unset( $toolbars['Full' ][2][$key] );
		}
	 
		// remove the 'Basic' toolbar completely
		unset( $toolbars['Basic' ] );
	 
		// return $toolbars - IMPORTANT!
		return $toolbars;
	}
	add_filter( 'acf/fields/wysiwyg/toolbars' , 'my_toolbars'  );

	/* SMTP CONFIG FOR PHP MAILER
	=================================================== */

	//add_action( 'phpmailer_init', 'configure_smtp' );
	function configure_smtp( PHPMailer $phpmailer )
	{
		$phpmailer->isSMTP(); //switch to smtp
		$phpmailer->Host       = 'bowser.quicktech.no';
		$phpmailer->SMTPAuth   = true;
		$phpmailer->Port       = 465;
		$phpmailer->Username   = 'sender@studentpsykologen.no';
		$phpmailer->Password   = 'sender_!@#$%*()';
		$phpmailer->SMTPSecure = 'ssl';
		$phpmailer->CharSet    = 'UTF-8';
//		$phpmailer->From = 'From Email Here';
//		$phpmailer->FromName='Sender Name';
		
		if( !$phpmailer->send() ) {
			echo "Mailer Error: " . $phpmailer->ErrorInfo;
		} else {
			echo "Message sent!";
		}                       

	}
?>
