<?php
get_header();

$menu_name = 'header-menu'; //e.g. primary-menu; $options['menu_choice'] in your case

if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) 
{
    $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
    $menu_items = wp_get_nav_menu_items( $menu->term_id );
}

//Now $menu_items is an object that contains all data for all menu items. So you can retrieve necessary data using foreach loop.
foreach ( $menu_items as $menu_item )
{
	$url = str_replace( array('#', '-page' ), '', $menu_item->url );

	if ( isset( $get_next_url ) && $get_next_url ) 
	{
		define( 'ARROW_LINK', $url ); 
		// echo '<h1>' . ARROW_LINK . ' - &2</h1>';
		$get_next_url = false;
	}

	if ( $url == 'header-top' ) 
	{ 
		if ( current( $menu_items ) < count($menu_items) )
		{
			$get_next_url = true;
		}
		else
		{
			$next_section_url = $url;
			echo '<h1>' . $next_section_url . ' - &2</h1>';
		}

		$url = 'home'; 
	}

	require_once ( 'page-' . ( $url == 'header-top' ? 'home' : $url ) . '.php' );
}

get_footer();
?>