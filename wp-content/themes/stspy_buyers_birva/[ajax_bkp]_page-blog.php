<!--=================================
     BLOG POST SECTION
==================================-->
<div id="blog-page">
	<div class="container">
		<!-- blog heading  -->
		<div class="sixteen columns menu-head">
			<h1><?php the_field( 'blog_title', '53' ); ?></h1>
			<p><?php the_field( 'blog_subtitle', '53' ); ?></p>
		</div>
		<div class="clear"></div>
		<div id="blog-wrapper" class="blog-main-wrapper">
			<!-- fullwidth -->
			<div id="status-blog" style="display: none;<em></em>"></div>

			<div  id="fullwidth" style="width:96%; margin: 0 auto !important; padding:20px 0 0px 0; display: block;">
				<?php
				// Set up the paged variable
				$paged = ( isset( $_GET['pg'] ) && (int)$_GET['pg'] > 0 ) ? (int)$_GET['pg'] : 1;
				query_posts( array( 'post_type' => 'post', 'paged' => $paged, 'posts_per_page' => get_field( 'blog_number_of_posts_per_page', '53' ), 'cat' => 1 ) );

				?>
				<?php if ( have_posts() ) : ?>
				    <?php while( have_posts() ) : the_post(); ?>
						<!-- blog post -->
						<article class="postlist postborder isotope-item" style="box-shadow: 0px 2px 3px #aaa; moz-ox-shadow: 0px 2px 3px #aaa; webkit-box-shadow: 0px 2px 3px #aaa;"> 
							<!-- content -->
							<div class="content-area">
								<!--thumb -->
								<?php 
								if ( get_field( 'blog_post_image' ) )
								{ 
								?>
								<div class="thumb">
									<a href="<?php the_permalink() ?>" class="fade">
										<span class="imageCover"></span>
										<img src="<?php echo get_field( 'blog_post_image' ); ?>">
									</a>
								</div>
								<?php
								}
								?>
								<!-- end thumb -->
								<!-- post detail -->
								<div class="postcontent">
									<div class="date"><div class="ribbon"><span><i class="icon-calendar"></i></span><?php echo date( 'j M, Y', strtotime( get_the_date() ) ) ?></div></div>
									&nbsp;
									<p class="posttitle"><a href="<?php the_permalink() ?>"><?php echo get_the_title() ?></a></p>
									<p><?php echo get_the_excerpt(); ?></p>
								</div>
								<!-- end post detail -->
								<!-- post meta -->
								<div class="post-meta">
									&nbsp;
									<a class="right" href="<?php the_permalink() ?>"><i class="icon-link"></i> Read more</a>
								</div>
								<!-- end post meta -->
							</div>
							<!-- end content -->
						</article>
						<!-- end blog post -->
				    <?php endwhile; ?>
				<?php endif; ?>
			</div>
			<!-- end fullwidth -->
			<div class="clearfix"></div>
		</div>
		<div class="clear"></div>
			<?php 
			if ( $wp_query->max_num_pages > 1 ) : ?>
		        <form id="pagination" class="pagination" method="POST" action="">
		        	<ul>
		        		<li><a href="#"><i class="icon-double-angle-left"></i></a></li>
						<li><a href="#"><i class="icon-angle-left"></i></a>
			            <?php for ( $i = 1; $i <= $wp_query->max_num_pages; $i ++ ) 
			            {
			                $link = $i == 1 ? remove_query_arg( 'pg' ) : add_query_arg( 'pg', $i );
			                echo '<li><a href="' . $link . '"' . ( $i == $paged ? ' class="current"' : '' ) . '>' . $i . '</a></li>';
			            }?>
        				<li><a href="#"><i class="icon-angle-right"></i></a></li>
						<li><a href="#"><i class="icon-double-angle-right"></i></a></li>
		            </ul>
		            <input style="visibility: hidden;" type="text" name="changedPage" value="1" style="" ></input>
		        </form>
		    <?php 
		   	endif;
			wp_reset_query();
		    ?>
		</div>
		<div class="clear"></div>
</div>
<!-- end Blog page -->
<div class="clearfix"></div>
<!-- parallax -->
<div id="parallax3" class="parallax">
	<div class="blog-teaser-bg parallax-bg">
		<div class="info-container">
			<div class="info">
				<div class="container">
					<div class="sixteen columns livet">
						<h2><?php the_field( 'blog_teaser_text', '53' ); ?></h2>
						<?php /* ::: <?php echo $_POST[ "changedPage" ]; ?> ::: <?php echo $_POST[ 'changedPage' ]; ?> ::: //Debug line backup */ ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end parallax -->
<div class="clear"></div>
<?php
if ( $_POST[ "changedPage" ] == "1" )
	{
		echo '<script type="text/javascript">'
		   , 	'$(document).ready( function() '
		   , 	'{'
		   , 		'$( "html, body" ).animate( { scrollTop: $( "#blog-wrapper" ).offset().top - 60 }, 500 );'
		   ,    '});'
		   , '</script>';
	}
	$_POST[ "changedPage" ] == "0";
?>